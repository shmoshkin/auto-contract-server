const R = require('ramda');
const jwt = require('jsonwebtoken');
const to = require('await-to-js').default;
const axios = require('axios');
const {getUserByEmail} = require('../query/auth');
const {getSelectResult, isError} = require('../utils/graphql');

const getTokenPayload = ({firstName, lastName, email, role}) => ({
  "name": `${firstName} ${lastName}`,
  "email": email,
  "role": role,
  "https://hasura.io/jwt/claims": {
    "x-hasura-allowed-roles": ["admin","user","lawyer"],
    "x-hasura-default-role": role,
    "x-hasura-role": role,
    "x-hasura-user-id": email,
    "x-hasura-custom": "group",
  }
})

const getToken = async (user, isAccessToken = true)  => {

    const token = await jwt.sign(
      getTokenPayload(user),    
      isAccessToken? process.env.SECRET : process.env.REFRESH_SECRET,
      { 
        algorithm: 'HS256'
      }
    )
    
  return token;
}

const getTokens = async (user) => {
  const accessToken = await getToken(user)

  return {accessToken}
}

const verifyToken = (token, isAccessToken = true) => jwt.verify(token,isAccessToken ? process.env.SECRET : process.env.REFRESH_SECRET)


module.exports = {
  getToken,
  verifyToken,
  getTokens
}