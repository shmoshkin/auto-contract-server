const R = require('ramda');

const getErrorMessage = (obj) => {
    const message = R.path(['data', 'errors', 0, 'message'], obj)
    return message ? message : 'Path not exist'
}

const getInsertResult = (obj, queryName) => {
    const result = R.path(['data', 'data', queryName, 'returning'], obj)
    return result ? result : 'Path not exist'
}

const getSelectResult = (obj, queryName) => {
    const result = R.path(['data', 'data', queryName], obj)
    return result ? result : 'Path not exist'
}

const isError = (obj) => R.path(['data', 'errors'], obj) ? true: false

module.exports = {
    getErrorMessage,
    getInsertResult,
    getSelectResult,
    isError
}