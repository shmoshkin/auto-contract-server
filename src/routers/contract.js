const contractRouter = require('express').Router();
const car = require('../bl/contract/car');
const appRental = require('../bl/contract/appRental');
const secret = require('../bl/contract/secret');
const partnership = require('../bl/contract/partnership'); 

contractRouter.get('/CAR_RENTAL/download/:id', function (req, res) {
    const params = req.params;
    car.download(params.id, res, false);
})

contractRouter.get('/CAR_RENTAL/translate/:id', function (req, res) {
    const params = req.params;
    car.download(params.id, res, true);
})

contractRouter.get('/APP_RENTAL/download/:id', function (req, res) {
    const params = req.params;
    appRental.download(params.id, res, false);
})

contractRouter.get('/APP_RENTAL/translate/:id', function (req, res) {
    const params = req.params;
    appRental.download(params.id, res, true);
})

contractRouter.get('/SECRET_CONTRACT/download/:id', function (req, res) {
    const params = req.params;
    secret.download(params.id, res, false);
})

contractRouter.get('/SECRET_CONTRACT/translate/:id', function (req, res) {
    const params = req.params;
    secret.download(params.id, res, true);
})

contractRouter.get('/PARTNERSHIP_CONTRACT/download/:id', function (req, res) {
    const params = req.params;
    partnership.download(params.id, res, false);
})

contractRouter.get('/PARTNERSHIP_CONTRACT/translate/:id', function (req, res) {
    const params = req.params;
    partnership.download(params.id, res, true);
})

contractRouter.get('/health', function(req, res) {
    res.sendStatus(200)
})

module.exports = contractRouter