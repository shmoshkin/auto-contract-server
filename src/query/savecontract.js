const save_app_rental = (app_rental_contract) => `mutation {
    insert_APP_RENTAL_CONTRACTS(objects: ["${app_rental_contract}"]) {
      returning {
        contract_id
      }
    }
  }`

  const save_car = (car_contract) => `mutation {
    insert_CAR_CONTRACTS(objects: ["${car_contract}"]) {
      returning {
        contract_id
      }
    }
  }`

  const save_partnership = (partnership_contract) => `mutation {
    insert_PARTNERSHIP_CONTRACTS(objects: ["${partnership_contract}"]) {
      returning {
        contract_id
      }
    }
  }`

  const save_secret = (secret_contract) => `mutation {
    insert_SECRET_CONTRACTS(objects: ["${secret_contract}"]) {
      returning {
        contract_id
      }
    }
  }`

  module.exports = {
    save_app_rental,
    save_car,
    save_partnership,
    save_secret
}