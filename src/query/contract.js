const getCarContractById = (id) => (
        `query MyQuery {
            CAR_CONTRACTS(where: {contract_id: {_eq: ${id}}}) {
              buyer_id
              buyer_name
              car_brand
              car_id
              car_is_hit
              car_km
              car_price
              car_test_end
              car_year
              contract_id
              date_of_sign
              delivery_date
              description
              owner_id
              owner_name
              payment_date
              place_of_sign
              status
              title
              type
              updated_at
              user_id
            }
          }`
)

const getAppRentalContractById = (id) => (
  `query MyQuery {
    APP_RENTAL_CONTRACTS(where: {contract_id: {_eq: ${id}}}) {
      app_city
      app_content_details
      app_level
      app_num
      app_rooms_details
      app_rooms_number
      app_street
      arnona
      bail_address
      bail_name
      contract_id
      date_of_enter
      date_of_leave
      date_of_sign
      description
      electricity
      internet
      is_parking_exists
      is_partners_exists
      is_partners_together
      is_pets_allowed
      is_warehouse_exists
      owner_id
      owner_name
      payment_period_in_months
      place_of_sign
      price_for_month
      renter_id
      renter_name
      status
      title
      tv
      type
      updated_at
      user_id
      vaad
      water
      partners_details
      clauses_in_contract {
        id
        clauseid {
          text
          id
        }
      }
    }
  }  
  `
)

const getPartnershipContractById = (id) => (
  `query MyQuery {
    PARTNERSHIP_CONTRACTS(where: {contract_id: {_eq: ${id}}}) {
      a_investment
      a_person_address
      a_person_id
      a_person_job
      a_person_name
      a_person_percents
      b_investment
      b_person_address
      b_person_id
      b_person_job
      years_of_no_competition
      b_person_name
      b_person_percents
      clauses_in_contract {
        clauseid {
          text
        }
      }
      contract_id
      court_for_contract
      date_of_sign
      description
      details_on_calc_books
      end_investment_date
      partnership_idea
      partnership_goals
      partnership_name
      status
      title
      type
      updated_at
      user_id
    }
  }`
)

const getSecretContractById = (id) => (
  `query MyQuery {
    SECRET_CONTRACTS(where: {contract_id: {_eq: ${id}}}) {
      city_of_courts
      commiter_city
      commiter_id
      commiter_name
      contract_id
      date_of_sign
      description
      invention_details
      inventor_id
      inventor_name
      status
      title
      type
      updated_at
      user_id
      violation_price
      clauses_in_contract {
        clauseid {
          text
        }
      }
    }
  }
  `
)

module.exports = {
  getCarContractById,
  getAppRentalContractById,
  getPartnershipContractById,
  getSecretContractById
}