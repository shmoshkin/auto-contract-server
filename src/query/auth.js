const createUser = (email, password, firstName, lastName) => `mutation {
    insert_USERS(objects: {email: "${email}", firstName: "${firstName}", lastName: "${lastName}", password: "${password}"}) {
      returning {
        created_at
        email
        firstName
        lastName
      }
    }
  }`

const getUserByEmail = (email) => `query getUserByEmail {
  USERS(where: {email: {_eq: "${email}"}}) {
    created_at
    email
    firstName
    lastName
    role
    password
  }
}`


const downloadContract = function(contract_id, contract_type) {
    if (contract_type == "car") {
        return `query downloadContract {
      CAR_CONTRACTS(where: {contract_id: {_eq: "${contract_id}"}}) {
        buyer_id
        buyer_name
        delivery_date
        car_brand
        car_id
        car_is_hit
        car_km
        car_price
        car_year
        car_test_end
        contract_id
        date_of_sign
        owner_id
        description
        owner_name
        payment_date
        place_of_sign
        status
        title
        type
        updated_at
        user_id
      }
    }`
    }

    if (contract_type == "app_rental") {
        return `query downloadContract {
      APP_RENTAL_CONTRACTS (where: {contract_id: {_eq: "${contract_id}"}}) {
        app_content_details
        app_rooms_details
        app_rooms_number
        bail_address
        bail_name
        contract_id
        date_of_enter
        date_of_leave
        description
        date_of_sign
        is_parking_exists
        is_partners_exists
        is_partners_together
        is_warehouse_exists
        is_pets_allowed
        owner_id
        owner_name
        payment_period_in_months
        period_in_months
        place_of_sign
        price_for_month
        renter_id
        renter_name
        status
        title
        type
        updated_at
        user_id
      }
    }`
    }

    if (contract_type == "partnership") {
        return `query downloadContract {
      PARTNERSHIP_CONTRACTS(where: {contract_id: {_eq: "${contract_id}"}}) {
        a_investment
        a_person_address
        a_person_id
        a_person_job
        a_person_name
        a_person_percents
        b_investment
        b_person_address
        b_person_id
        b_person_job
        b_person_name
        b_person_percents
        contract_id
        court_for_contract
        date_of_sign
        description
        details_on_calc_books
        end_investment_date
        partnership_idea
        partnership_goals
        partnership_name
        status
        title
        type
        updated_at
        user_id
      }
    }`
    }

    if (contract_type == "secret") {
        return `query downloadContract {
      SECRET_CONTRACTS(where: {contract_id: {_eq: "${contract_id}"}}) {
        city_of_courts
        commiter_city
        commiter_id
        commiter_name
        contract_id
        date_of_sign
        description
        invention_details
        inventor_id
        inventor_name
        status
        title
        type
        updated_at
        user_id
        violation_price
      }
    }`
    }
}

const getClauses = function(contract_id) {
    return `query getClauses {
    
    CLAUSES_FOR_SUGGEST_IN_CONTRACT(where: {contract_id: {_eq: "${contract_id}"}}) {
    
      clauseid{  
        contract_type 
        id 
        tag 
        text}
      
    }
  }`

}

const getPartners = function(contract_id) {
    return `query getPartners {
    APP_RENTAL_PARTNERS(where: {contract_id: {_eq: "${contract_id}"}}) {
      contract_id
      partner_id
      partner_room
      partner_name
      price
    }
  }`

}
const getPayments = function(contract_id) {
    return `query getPayments {
    APP_RENTAL_PAYMENTS(where: {contract_id: {_eq: "${contract_id}"}, _and: {is_renter_pay: {_eq: true}}}) {
      contract_id
      is_renter_pay
      kind_of_payment
    }
  }`

}
module.exports = {
    createUser,
    getUserByEmail,
    downloadContract,
    getClauses,
    getPartners,
    getPayments
}