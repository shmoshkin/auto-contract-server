const bcrypt = require('bcrypt');
const axios = require("axios")
const to = require('await-to-js').default;
const R = require('ramda');
const {getUserByEmail} = require('../../query/auth')
const {isError, getSelectResult} = require('../../utils/graphql')
const {getTokens} = require('../../utils/auth')

const signIn = async (email, password, res) => {
  
  const [err,userRes] = await to(axios({
    url: process.env.HASURA_ENDPOINT,
    method: 'post',
    headers: {
      'x-hasura-admin-secret': process.env.ADMIN_SECRET
    },
    data: {
      query: getUserByEmail(email)
    }
  }))
  
  if (err || isError(userRes) || R.isNil(getSelectResult(userRes, "USERS")[0])){
    return res.status(401).json({message: "User not found"})
  } else {  
    const user = getSelectResult(userRes, "USERS")[0]
    
    const match = await bcrypt.compare(password, user.password);

    if(!match) {
      return res.status(401).json({message: "Password incorrect"})
    } else {
      
      const tokens = await getTokens(user);

      return res.status(200).json({
        message: "Sign in successfully",
        token: tokens.accessToken
      })
    }
  }
}

module.exports = {
    signIn
}