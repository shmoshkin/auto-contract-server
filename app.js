require('dotenv').config();
const logger = require('morgan');
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');

const { verifyToken } = require('./src/utils/auth');
const { getUserByEmail, getClauses, getPartners, getPayments } = require('./src/query/auth');
const { downloadContract } = require('./src/query/auth');
const { getSelectResult, isError } = require('./src/utils/graphql');
const app = express()

const axios = require("axios")
const to = require('await-to-js').default;

var fs = require('fs');

app.use(cors({ credentials: true, origin: process.env.CORS }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger());
app.use(cookieParser());

app.get('/health', function(req, res) {
    const checkEnv = `${process.env.HASURA_ENDPOINT}
    ${process.env.LOCALHOST}
    ${process.env.PORT}
    ${process.env.CORS}
    ${process.env.SECRET}
    ${process.env.ADMIN_SECRET}
    ${process.env.REFRESH_SECRET}`
    res.status(200).json({message: checkEnv})
})

app.get('/downloadContract/:contract_type/:contract_id', function(req, res) {
    const params = req.params;
    const downloadContract2 = async(contract_id, contract_type, res2) => {
        const [err, contractRes] = await to(axios({
            url: process.env.HASURA_ENDPOINT,
            method: 'post',
            headers: {
                'x-hasura-admin-secret': process.env.ADMIN_SECRET
            },
            data: {
                query: downloadContract(contract_id, contract_type)
            }
        }))

        if (err || isError(contractRes)) {
            return res.status(404).json({ message: "Error" })
        } else {

            try {
                var data = fs.readFileSync('./src/contracts/' + contract_type + '.txt', 'utf8');
                contract_string = data.toString()

                for (var j in contractRes.data.data) {
                    for (var i in contractRes.data.data[j][0]) {
                        contract_string = contract_string.replace("param_" + i, contractRes.data.data[j][0][i]);
                    }
                }

                const [err, clausesres] = await to(axios({
                    url: process.env.HASURA_ENDPOINT,
                    method: 'post',
                    headers: {
                        'x-hasura-admin-secret': process.env.ADMIN_SECRET
                    },
                    data: {
                        query: getClauses(contract_id)
                    }
                }))

                clauses_string = "";
                for (var i in clausesres.data.data.CONTRACT_CLAUSES) {
                    clauses_string += clausesres.data.data.CONTRACT_CLAUSES[i].text + "\n";
                }

                contract_string = contract_string.replace("param_clauses", clauses_string);

                const [err1, partnersres] = await to(axios({
                    url: process.env.HASURA_ENDPOINT,
                    method: 'post',
                    headers: {
                        'x-hasura-admin-secret': process.env.ADMIN_SECRET
                    },
                    data: {
                        query: getPartners(contract_id)
                    }
                }))

                partners_string = "";
                for (var i in partnersres.data.data.APP_RENTAL_PARTNERS) {
                    partners_string += "שם השותף - " + partnersres.data.data.APP_RENTAL_PARTNERS[i].partner_name + ", " +
                        "תעודת זהות - " + partnersres.data.data.APP_RENTAL_PARTNERS[i].partner_id + ", " +
                        "חדר - " + partnersres.data.data.APP_RENTAL_PARTNERS[i].partner_room + ", " +
                        "מחיר - " + partnersres.data.data.APP_RENTAL_PARTNERS[i].price + "\n";
                }

                contract_string = contract_string.replace("param_partners_details", partners_string);


                const [err2, paymentsres] = await to(axios({
                    url: process.env.HASURA_ENDPOINT,
                    method: 'post',
                    headers: {
                        'x-hasura-admin-secret': process.env.ADMIN_SECRET
                    },
                    data: {
                        query: getPayments(contract_id)
                    }
                }))

                payments_string = "";
                for (var i in paymentsres.data.data.APP_RENTAL_PAYMENTS) {
                    payments_string += paymentsres.data.data.APP_RENTAL_PAYMENTS[i].kind_of_payment + ", "
                }

                contract_string = contract_string.replace("param_payment_renter", payments_string);


                res.status(200).json({ data: contract_string })
            } catch (e) {
                console.log('Error:', e.stack);
            }

        }


    }
    downloadContract2(params.contract_id, params.contract_type, res)
})


app.get('/translateContract/:contract_type/:contract_id', function(req, res) {
    const params = req.params;

    const translateContract2 = async(contract_id, contract_type, res2) => {
        const [err, contractRes] = await to(axios({
            url: process.env.HASURA_ENDPOINT,
            method: 'post',
            headers: {
                'x-hasura-admin-secret': process.env.ADMIN_SECRET
            },
            data: {
                query: downloadContract(contract_id, contract_type)
            }
        }))


        if (err || isError(contractRes)) {
            return res.status(401).json({ message: "error" })
        } else {

            try {
                var data = fs.readFileSync('./src/contracts/' + contract_type + '_translate.txt', 'utf8');
                contract_string = data.toString()

                for (var j in contractRes.data.data) {
                    for (var i in contractRes.data.data[j][0]) {
                        contract_string = contract_string.replace("param_" + i, contractRes.data.data[j][0][i]);
                    }
                }

                res.status(200).json({ data: contract_string })
            } catch (e) {
                console.log('Error:', e.stack);
            }

        }


    }
    translateContract2(params.contract_id, params.contract_type, res)
})

app.use('/auth', require('./src/routers/auth'))
app.use('/contract', require('./src/routers/contract'))


app.use(async(req, res, next) => {

    const accessToken = req.cookies["access-token"] || req.headers.Authorization;

    if (!accessToken) return res.sendStatus(401);

    try {
        const { data: { email } } = verifyToken(accessToken)
        req.userId = email;
        return next();
    } catch {}

    return next(req, res);
})



app.listen(process.env.PORT, () => console.log(`Hozelito Server Listening at http://localhost:${process.env.PORT}`))